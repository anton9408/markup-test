// Обработка отсутствующего console.log
	if(window.console === undefined) {
		window.console = {
			log: function () {
				return false;
			}
		}
	}

// Кроссбраузерный requestAnimationFrame
 	window.requestAnimFrame =
		window.requestAnimationFrame       ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function(callback, element){
     callback();
    };

// Сообщаяем блокам о готовности API карт
	ymapAPIready = false;

	if(typeof ymaps !== 'undefined') {
		ymaps.ready(function () {
			ymapAPIready = true;
			$(document).trigger('ymapAPIready');
		});
	}

// Предотвращаем всплытие блочных событий
	$(function () {
		$('.bem').on('resize.block', function (e) {
			e.stopPropagation();
		})
	});
//
// Узнаем ширину скролла
	$(function getScrollBarWidth () {
    var inner = document.createElement('p');
    inner.style.width = '100%';
    inner.style.height = '200px';

    var outer = document.createElement('div');
    outer.style.position = 'absolute';
    outer.style.top = '0px';
    outer.style.left = '0px';
    outer.style.visibility = 'hidden';
    outer.style.width = '200px';
    outer.style.height = '150px';
    outer.style.overflow = 'hidden';
    outer.appendChild (inner);

    document.body.appendChild (outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2)
        w2 = outer.clientWidth;
    document.body.removeChild (outer);


		$('head').append('<style type="text/css">.scroll-fixer{margin-right: -' + (w1 - w2) + 'px;overflow-y: scroll;}</style>');
		$('head').append('<style type="text/css">.scroll-fixer-pos{right: -' + (w1 - w2) + 'px;overflow-y: scroll;}</style>');
		$('head').append('<style type="text/css">html.fancybox-margin .fancyfixer{right: ' + (w1 - w2) + 'px;}</style>');
		// .fancyfixer на фиксированно спозиционированные элементы для ремона fancybox
	});

// Предобработка ресайза, для отсечения лишних событий (в основном для мобил)
	(function () {
		var windowWidth = $(window).width();

		$(window).on('resize', function () {
			var newWindowWidth = $(window).width();
			if(newWindowWidth != windowWidth) {
				windowWidth = newWindowWidth;
				$(window).trigger('resizeWidth');
			}
		});
	})();

// Функции WP
	var Webprofy = {
		toggleLabel : function($elements, enable){
			var disabled = !enable;
			$elements.each(function(){
				$(this).toggleClass('disabled', disabled).find('input').prop('disabled', disabled);
			});
			return this;
		},
		setInputStatus : function($elements, ok, message){
			var html = '<span class="status-' + (ok ? 'ok' : 'error') + '">' + (message || '') + '</span>';
			$elements.each(function(){
				$(this).closest('.field').find('.status').html(html);
			});
			return this;
		},
		oldIeCheck : $('html').hasClass('bx-ie9') || $('html').hasClass('bx-ie8'),

		isOldIe: function(){
			return this.oldIeCheck;
		}
	};

	function initSliders(){
		$('.bxslider').bxSlider({
			pager: false,
			adaptiveHeight: true
		});
	}

	sameheight = function(container){
		var currentTallest = 0,
			currentRowStart = 0,
			rowDivs = new Array(),
			$el,
			topPosition = 0;
		$(container).each(function() {
			$el = $(this);
			$($el).height('auto')
			topPostion = $el.position().top;

			if (currentRowStart != topPostion) {
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
				}
				rowDivs.length = 0; // empty the array
				currentRowStart = topPostion;
				currentTallest = $el.height();
				rowDivs.push($el);
			} else {
				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
		});
	}

	$(window).load(function() {
	  sameheight('.is-sameheight');
	});


	$(window).resize(function(){
	  sameheight('.is-sameheight');
	});


	$.fn.promoSlider = function(_options) {

		var options = $.extend({
			rotate: true,
			rotateSpeed: 5000,
			transitionTime: 1000,
			transition: function(olditem, newitem, step){
				newitem.css({'opacity': step});
			},
			transitionStart: function(olditem, newitem){
				// do nothing
			},
			transitionEnd: function(olditem, newitem){
				// do nothing
			}
		}, _options);

		if(typeof(this.data('promoSlider')) != "undefined"){
			return this.data('promoSlider');
		} else {
			return this.each(function(){

				var promo = $(this);
				var items = promo.find('.main-promo-item');
				var current = items.filter('.active');
				if(current.length != 1){
					current = items.eq(0);
				}

				var timeout;

				// Reset active and create pagination

				items.removeClass('active');
				current.addClass('active');
				promo.find('.promo-slider-paginator').remove();
				promo.append('<ul class="promo-slider-paginator"></ul>');
				var paginator = promo.find('.promo-slider-paginator');
				items.each(function(){
					if($(this).is('.active')){
						paginator.append('<li class="active"></li>');
					} else {
						paginator.append('<li></li>');
					}
				});

				// Set Height
				promo.find('.promo-slider').height(current.height());

				// Set Rotation
				if(options.rotate){
					timeout = setInterval(next, options.rotateSpeed);
				}

				function getNext(){
					var index = items.index(items.filter('.active'));
					index++;
					if(index >= items.length)
						index = 0;
					return index;
				}

				function getPrev(){
					var index = items.index(items.filter('.active'));
					index--;
					if(index < 0)
						index = items.length - 1;
					return index;
				}

				function switchTo(i){
					items.filter('.active').addClass('transition-from');
					items.eq(i).addClass('transition-to');
					var olditem = items.filter('.active');
					var newitem = items.eq(i);
					options.transitionStart(olditem, newitem);
					$({'step': 0}).animate({'step': 1}, {
						duration: options.transitionTime,
						step: function(val){
							options.transition(olditem, newitem, val);
						},
						complete: function(){
							options.transitionEnd(olditem, newitem);
							items.removeClass('active transition-from transition-to').eq(i).addClass('active');
							paginator.find('li').removeClass('active').eq(i).addClass('active');
						}
					});
				}

				function next(){
					switchTo(getNext());
				}
				function prev(){
					switchTo(getPrev());
				}

				var promoSlider = {
					next: next,
					prev: prev
				}

				$(this).data('promoSlider', promoSlider);
			});
		}
	};

	function initClickToChange(){
		$(document).on('click', '.click-to-change-link', function(e){
			e.preventDefault();
			var ctc = $(this).parents('.click-to-change');
			ctc.find('input[name="'+ctc.data('state-input')+'"]').val("1");
			ctc.find('.state-view').hide();
			ctc.find('.state-edit').show();
		});
		$(document).on('click', '.click-to-cancel-link', function(e){
			e.preventDefault();
			var ctc = $(this).parents('.click-to-change');
			ctc.find('input[name="'+ctc.data('state-input')+'"]').val("");
			ctc.find('.state-view').show();
			ctc.find('.state-edit').hide();
		});
	}



	function initSidebar(){
		$('header .menu-button').on('click', function(e){
			$('body').toggleClass('sidebar-opened');
			e.preventDefault();
		});
		$('.container').on('click', function(e){
			if($(e.target).is('.menu-button'))
				return;
			if($('body').is('.sidebar-opened')){
				$('body').removeClass('sidebar-opened');
			}
		});
	}

	function initDropdownMenu(){
		var WAIT_TIME = 500;

		function closeAll(){
			$('.header-dropdown-menu').removeClass('opened');
			$('header').removeClass('dropdown-opened');
			$('.header-menu a').find('span').css({'width': 0});
		}

		$('.header-menu a[data-dropdown-menu]').each(function(){
			var dropdown = $('#' + $(this).data('dropdown-menu'));
			var header = $(this).parents('header');

			$(this).on('mouseover', function(){
				if(!dropdown.is('.opened')){
					timestart = new Date().getTime();
					$(this).find('span').stop(true).animate({'width': '100%'}, WAIT_TIME, function(){
						$('.header-dropdown-menu').removeClass('opened');
						dropdown.addClass('opened');
						header.addClass('dropdown-opened');
						$('.header-menu a').find('span').css({'width': 0});
						$(this).css({'width': '100%'});
					});
				}
			});
			$(this).on('mouseout', function(){
				if(!dropdown.is('.opened')){
					$(this).find('span').stop(true).css({'width': '0'});
				}
			});
		});
		$('header').on('mouseleave', function(){
			closeAll();
		});
		$('header .menu-button').on('click', function(){
			closeAll();
		});
	}

	function initFixedHeader() {
		var headerBottom = $('.header-bottom');
	    $(window).scroll(function() {
	        if ($(this).scrollTop() > 1) {
	            headerBottom.addClass("fixed");
	        }
	        else {
	            headerBottom.removeClass("fixed");
	        }
	    });
	}

	$.fn.selectRange = function(start, end) {
	    if(!end) end = start;
	    return this.each(function() {
	        if (this.setSelectionRange) {
	            this.focus();
	            this.setSelectionRange(start, end);
	        } else if (this.createTextRange) {
	            var range = this.createTextRange();
	            range.collapse(true);
	            range.moveEnd('character', end);
	            range.moveStart('character', start);
	            range.select();
	        }
	    });
	};

	function initFilter(){
		function numberWithSpaces(x) {
			return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		}
		function formatPrice(x, significant_digits, convert_factor) {

			if(!convert_factor){
				convert_factor = 1;
			}
			x = Math.round(x * convert_factor);

			var digits = x.toString().length;
			var divider = 1;
			if(digits > significant_digits){
				divider = Math.pow(10, digits - significant_digits);
			}

			x = x - (x % divider);
			return numberWithSpaces(x);
		}
		function bound(_number, _min, _max){
			return Math.max(Math.min(_number, _max), _min);
		}

		$('.bx_filter').each(function(){
			var CF = $(this);

			// Range control
			CF.find('.numbers-range-control').each(function(){
				var RC = $(this);
				var log = (RC.data('log') == '1');
				var field_min = RC.find('.range-min');
				var field_max = RC.find('.range-max');
				var realfield_min = $('#' + field_min.data('name') );
				var realfield_max = $('#' + field_max.data('name') );
				var data_min = RC.data('min');
				var data_max = RC.data('max');
				var significant_digits = RC.data('significant-digits') | 3;
				var convert_factor = RC.data('convert-factor') | 1;

				console.log("Convert factor", convert_factor);

				function updateRealFields(){
					realfield_min.val(field_min.val().toString().replace(/[^\d]/g, '') / convert_factor);
					realfield_max.val(field_max.val().toString().replace(/[^\d]/g, '') / convert_factor);
					realfield_min.change();
					realfield_max.change();
				}

				function updateFields(values){
					if(log){
						var min = Math.exp(values[0]);
						var max = Math.exp(values[1]);
						min = bound(min, data_min, data_max);
						max = bound(max, data_min, data_max);
						field_min.val(formatPrice(min, significant_digits, convert_factor));
						field_max.val(formatPrice(max, significant_digits, convert_factor));
					} else {
						field_min.val(formatPrice(values[0], significant_digits, convert_factor));
						field_max.val(formatPrice(values[1], significant_digits, convert_factor));
					}
					updateRealFields();
				}

				function updateSlider(onchange){
					var min = field_min.val().toString().replace(/[^\d]/g, '') / convert_factor;
					var max = field_max.val().toString().replace(/[^\d]/g, '') / convert_factor;

					min = parseInt(min, 10);
					max = parseInt(max, 10);

					if(isNaN(min) || isNaN(max))
						return;

					if(onchange){
						if(max < min) {
							max = Math.round(min * 1.1);
						}
						min = bound(min, data_min, data_max);
						max = bound(max, data_min, data_max);

					}

					field_min.val(numberWithSpaces(min * convert_factor));
					field_max.val(numberWithSpaces(max * convert_factor));

					updateRealFields();

					if(log){
						RC.find('.range-slider').slider({"values": [Math.log(min), Math.log(max)], range: true,
							min: min,
							max: max});
					} else {
						RC.find('.range-slider').slider({"values": [min, max], range: true,
							min: min,
							max: max});
					}
				}

				if(parseInt(data_min) == 0 && log){
					data_min = 1; // Logaryphmic slider can't handle values < 1
				}
				if(parseInt(data_max) == 0){
					console.log('Zero price slider');
					return;
				}

				var options = {
					range: true,
					max: data_max,
					min: data_min,
					values: [RC.data('left'), RC.data('right')],
					step: 1,
					slide: function(e, ui){
						updateFields(ui.values);
					}
				}

				if(log){
					options.step = 0.00001;
					options.max = Math.log(options.max) + options.step;
					options.min = Math.log(options.min) - options.step;
					options.values = [Math.log(options.values[0]), Math.log(options.values[1])];
				}

				RC.find('.range-slider').slider(options);
				updateFields(options.values);

				/* RC.find('.range-min, .range-max').on('keydown', function(e){
					if(e.which > 57){
						e.preventDefault();
						return false;
					}
				}); */

				RC.find('.range-min, .range-max').on('keyup', function(e){
					console.log(e.which);
					if(e.which > 47 || e.which == 8){
						 // store current positions in variables
						var start = this.selectionStart;
						var l = this.value.length;
						updateSlider();
						if(this.value.length != l){
							start += (this.value.length - l);
						}
						this.setSelectionRange(start, start);
					}
				});
				RC.find('.range-min, .range-max').on('change blur', function(e){
					updateSlider(true);
				});
			});
		});
	}

	function customSliderItemsSlideOut(items, step){
		var TIMEALIVE = 0.3;
		var length = items.length;
		var timestep = (1 - TIMEALIVE) / length;

		items.each(function(key){
			var resampledstep = (step - timestep * key) / TIMEALIVE;
			if(resampledstep < 0)
				resampledstep = 0;
			if(resampledstep > 1)
				resampledstep = 1;
			$(this).css({
				'opacity': (1 - resampledstep),
				'left': (- resampledstep * 200) + 'px'
			});
		});
	}

	function customSliderItemsSlideIn(items, step){
		var TIMEALIVE = 0.4;
		var length = items.length;
		var timestep = (1 - TIMEALIVE) / length;

		items.each(function(key){
			var resampledstep = (step - timestep * key) / TIMEALIVE;
			if(resampledstep < 0)
				resampledstep = 0;
			if(resampledstep > 1)
				resampledstep = 1;
			$(this).css({
				'opacity': resampledstep,
				'top': ((1 - resampledstep) * 200) + 'px'
			});
		});
	}

	function customSliderTransition(olditem, newitem, step){
		if(step < 0.33333){
			step = step * 3; // map back to 0-1
			customSliderItemsSlideOut(olditem.find('.promo-text > div'), step);
		} else if (step < 0.66666){
			step = (step-0.33333) * 3; // map back to 0-1
			newitem.css({'opacity': (step-0.25) * 2});
		} else {
			step = (step-0.66666) * 3; // map back to 0-1
			//newitem.find('.promo-text').css({'opacity': (step-0.75) * 4});
			customSliderItemsSlideIn(newitem.find('.promo-text > div'), step);
		}
	}

	function initForms(){
		// Checkboxes
		$(document).find('input[type="checkbox"], input[type="radio"]').not('.unstyled').each(function(){
			$(this).iCheck();
			$(this).on('ifToggled', function(e){
				$(this).trigger('change').trigger('click'); // Trigger default event
			});
		});

		$(document).on('comparePageReloaded', function(){
			$('#bx_catalog_compare_block').find('input[type="checkbox"], input[type="radio"]').not('.unstyled').each(function(){
				$(this).iCheck();
				$(this).on('ifToggled', function(e){
					$(this).trigger('change').trigger('click'); // Trigger default event
				});
			});
		});
	}

	function initOrderForm(){
		$('.order-page').each(function(){
			var $page = $(this),
				payway = new function(){
					var $payway = $page.find('.payway-holder'),
						$radios = $payway.find('.payway-radio');

					$.extend(this, {
						enableForDelivery : function(delivery){
							var changeValue = false;
							$radios.each(function(){
								var $radio = $(this),
									deliveryValues = eval($radio.attr('data-deliveries')),
									disabled = ($.inArray(delivery, deliveryValues) == -1);
								Webprofy.toggleLabel($radio.closest('label'), !disabled);
								if(disabled && $radio.is(':checked')){
									changeValue = true;
									$radio.prop('checked', false);
								}
							});

							if(changeValue){
								$radios.filter(':enabled:first').prop('checked', true);
							}
						}
					});
				},
				delivery = new function(){
					var $holder = $page.find('.delivery-holder').eq(0),
						$showPickup = $page.find('.show-pickup'),
						$hidePickup = $page.find('.hide-pickup'),
						pickupValues = eval($holder.attr('data-pickup-values')),
						$radios = $holder.find('.delivery-radio').change(function(){
							var $radio = $(this),
								value = $radio.val(),
								isPickup = $.inArray(value, pickupValues) > -1;
							$showPickup.toggle(isPickup);
							$hidePickup.toggle(!isPickup);
							payway.enableForDelivery(value);
						}),
						$radio = $radios.filter(':checked');

					if(!$radio.length){
						$radio = $radios.eq(0).prop('checked', true);
					}
					$radio.trigger('change');
				}
		});
	}

	var wpCart = new function(){
		var self = this;
		this.showPopupItemAdded = function(item){
			$.fancybox(
				Mustache.render(this.popupTemplate, {
					item: item,
					cart_url: "/cart/",
					messages: self.messages
				})
			);
			BX.onCustomEvent(window,'OnBasketChange');
		}

		this.popupTemplate = '' +
		'<div class="item-added-to-cart">' +
			'<div class="item-added-action-title">{{messages.item_added_to_cart}}</div>' +
			'<div class="item-added-image"><img src="{{item.image}}" /></div>' +
			'<div class="item-added-title">{{item.title}}</div>' +
			'<div class="buttons">' +
				'<a href="#" class="close button">{{messages.continue_shopping}}</a> ' +
				'<a href="{{cart_url}}" class="button go-to-cart">{{messages.go_to_cart}}</a>' +
			'</div>' +
		'</div>';

		this.messages = {
			item_added_to_cart: 'Товар добавлен в корзину',
			continue_shopping: 'Продолжить покупки',
			go_to_cart: 'Перейти в корзину'
		}
	}

	// Вариант с зумом
	function initCatalogGallery(){
		$('.image-gallery-slider-with-thumbnails').each(function(){
			var gallery = $(this);
			var items = gallery.find('.more-images li');
			var container = gallery.find('.image');
			var image = container.find('img');

			if(image.data('fullimage') != image.attr('src')){
				image.addClass('fancyzoom');
			}

			items.on('click', function(){
				if(!$(this).is('.active')){
					$(this).addClass('active').siblings().removeClass('active');
					var img = $(this).find('img');
					container.addClass('loading');
					image.one('load', function(){
						container.removeClass('loading');
					});
					image.attr('src', img.data('image'));
					image.attr('data-fullimage', img.data('fullimage'));

					if(image.data('fullimage') != image.attr('src')){
						image.addClass('fancyzoom');
					} else {
						image.removeClass('fancyzoom');
					}
				}
			});
		});

		$(document).on('mouseover', '.image-gallery-slider-with-thumbnails .image img.fancyzoom', function(e){
			$('.fancyview-zoom').remove();
			var zoom = $(this).clone();
			zoom.attr('src', zoom.data('fullimage'));
			zoom.insertAfter($(this)).wrap('<div class="fancyview-zoom"></div>');

			$(document).one('mouseout', '.image-gallery-slider-with-thumbnails .image', function(e){
				//console.log('remove');
				$('.fancyview-zoom').remove();
			});

		});
		$(document).on('mousemove', '.image-gallery-slider-with-thumbnails .image img.fancyzoom', function(e){
			if($('.fancyview-zoom').length == 0){
				$(this).mouseover();
			}

			if(!e.offsetX){
				posx = e.pageX-$(this).offset().left;
				posy = e.pageY-$(this).offset().top;
			} else {
				posx = e.offsetX;
				posy = e.offsetY;
			}

			percentx = posx / $(this).parent().width()
			percenty = posy / $(this).parent().height()

			$('.fancyview-zoom').css({
				left: (posx - $('.fancyview-zoom').width() / 2) + 'px',
				top: (posy - $('.fancyview-zoom').height() / 2) + 'px'
			});

			$('.fancyview-zoom img').css({
				left: ($('.fancyview-zoom').width() / 2 - percentx * $('.fancyview-zoom img').width()) + 'px',
				top: ($('.fancyview-zoom').height() / 2 - percenty * $('.fancyview-zoom img').height()) + 'px'
			});
		});
	}

	// Вариант без зума
	function initCatalogGallery2(){
		$('.image-gallery-slider-with-thumbnails').each(function(){
			var gallery = $(this);
			var items = gallery.find('.more-images li');
			var container = gallery.find('.image');
			var image = container.find('img');

			if(image.data('fullimage') != image.attr('src')){
				image.addClass('fancyzoom');
			}

			items.on('click', function(){
				if(!$(this).is('.active')){
					$(this).addClass('active').siblings().removeClass('active');
					var img = $(this).find('img');
					container.addClass('loading');
					image.one('load', function(){
						container.removeClass('loading');
					});
					image.attr('src', img.data('image'));
					image.attr('data-fullimage', img.data('fullimage'));

					/* if(image.data('fullimage') != image.attr('src')){
						image.addClass('fancyzoom');
					} else {
						image.removeClass('fancyzoom');
					} */
				}
			});

			image.on('click', function(){
				var links = [];
				items.each(function(){
					links.push($(this).find('img').data('fullimage'));
				});
				var index = items.index(items.filter('.active'));
				links = links.concat(links.splice(0,index));
				$.fancybox(links);
			});

		});
	}

	$(function(){
		// initSliders();
		// initClickToChange();
		// initShowOnCondition();
		// initSidebar();
		// initDropdownMenu();
		// initFilter();
		// initForms();
		// initOrderForm();

		initCatalogGallery();

		$('.main-promo').promoSlider({
			transition: customSliderTransition,
			transitionStart: function(olditem, newitem){
				newitem.css({opacity: 0});
				//newitem.find('.promo-text').css({'opacity': 1});
				newitem.find('.promo-text > div').css({'opacity': 0, 'top': '200px'});
			},
			transitionEnd: function(olditem, newitem){
				//olditem.find('.promo-text').css({'opacity': 1});
				olditem.find('.promo-text > div').css({'opacity': 1, 'left': 0});
				//newitem.find('.promo-text').css({'opacity': 1});
			},
			transitionTime: 2000
		});

		$('.catalog-sort-and-view select').each(function(){
			$(this).select2({
				minimumResultsForSearch: 20
			});
		});
		$('.bx_filter select').select2({
			minimumResultsForSearch: 20
		});
	});
