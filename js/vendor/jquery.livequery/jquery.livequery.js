// Livequery
	(function($){
		var cache = [];
		$.fn.livequery = function (callback) {
			function checkBlocks() {
				var $context = $(this);
				var _tmp;
				var length = cache.length;

				for(var i = 0; i < cache.length;i++) {
					_tmp = $context.find(cache[i].selector);

					if(_tmp.length > 0 && !_tmp.data('live-checker')) {
						_tmp.each(function() {
							errorCatcher(cache[i].callback, this);
							$(this).data('live-checker', true);
						});
					}
				}
			}

			function errorCatcher (callback, block) {
				try {
					callback.apply(block);
				} catch (e) {
					console.warn('Ошибка в поведении блока: ' + block.selector, e.stack);
				}
			}



			if(callback != null) {
				cache.push({
					selector: this.selector,
					callback: callback
				});


				return this.each(function () {
					var $elem = $(this);

					errorCatcher(callback, this);
					$elem.data('live-checker', true);
				});
			} else {
				return this.each(checkBlocks);
			}
		}
	})(jQuery);
