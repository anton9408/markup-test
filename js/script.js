function startPreventBodyScroll(){
	$('body').addClass('is-fixed');
	if($(window).width() < 768){
		$('body').css({'position': 'fixed', width: $('body').width() + 'px'});
	}

}
function endPreventBodyScroll(){
	$('body').removeClass('is-fixed');
	if($(window).width() < 768){
		$('body').css({'position': 'static', 'width': 'auto'});
	}
}

// Переносит картинки в бэкграунд родителя
function jsBgImage(){
	$('.js-bg-image').each(function(){
		$this = $(this);
		$src = $this.attr('src');
		if($this.data('at-2x') && window.devicePixelRatio > 1.5){
			$src = $(this).data('at-2x');
		}
		$this.parent().css({ 'background-image': 'url("' + $src + '")'});
		$this.hide();
	});
}

$(function(){
	jsBgImage();
});

// Скрипт ресайза
/*
  $(function() {
		var mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
		if(!mobile) return false;

  	var scalableWidth = 0;
		var currentOrientation;

  	 var screenWidth = Math.min(screen.width, screen.height);
  	 var screenHeight = Math.max(screen.width, screen.height);

  	function checkDeviceOrientation () {
	  	if(window.matchMedia) {
	  	 	currentOrientation = window.matchMedia("(orientation: portrait)").matches ? 'portrait' : 'landscape';
	  	}
	  	else {
	  		$("#viewport").attr("content","width=device-width, initial-scale=1");
	  	 	currentOrientation = $(window).width() < $(window).height() ? 'portrait' : 'landscape';
	  	}
  	}

  	function checkDPRedValues() {
			var dpr;
			var isPhisical;

			if (window.devicePixelRatio) {
			dpr = window.devicePixelRatio;
			} else {
			dpr = 1
			}

			if (currentOrientation == 'portrait') {
			isPhisical = Math.abs((screenWidth / dpr) - window.innerWidth) < 50;
			} else {
			isPhisical = Math.abs((screenHeight / dpr) - window.innerWidth) < 50;
			}

	  	if(isPhisical) {
	  		screenWidth = screenWidth / dpr;
	  		screenHeight = screenHeight / dpr;
	  	}
	  }

  	function resizeViewport () {
			if(currentOrientation == 'landscape') {
				if(screenHeight>scalableWidth) {
					$("#viewport").attr("content","width=1024, initial-scale="+(screenHeight/1024) +', minimum-scale=' + (screenHeight/1024-0.1));
					$(window).resize();
				} else {
					$("#viewport").attr("content","width=device-width, initial-scale=1, user-scalable=no");
					$(window).resize();
				}
			} else {
				if(screenWidth>scalableWidth) {
					$("#viewport").attr("content","width=1024, initial-scale="+(screenWidth/1024) +', minimum-scale=' + (screenWidth/1024-0.1));
					$(window).resize();
				} else {
					$("#viewport").attr("content","width=device-width, initial-scale=1, user-scalable=no");
					$(window).resize();
				}
			}
		}

  	(function () {
  		var _temp = currentOrientation;

	  	if(window.addEventListener){
				window.addEventListener('orientationchange', function () {
					var counter = 0;
					_temp = currentOrientation;

					var intervalID = setInterval(function () {
						checkDeviceOrientation();
						if(currentOrientation == _temp && counter <5) {
							counter++;
							return false;
						} else if(counter >=5) {
							clearInterval(intervalID);
						} else {
							checkDeviceOrientation();
							resizeViewport();
							clearInterval(intervalID);
						}
					}, 200);
				});
			}
		})();

		checkDeviceOrientation();
		checkDPRedValues();
		resizeViewport();
	});
*/
/*
	// Прижимаем футер
	$(function () {
		var $html = $('html');
		var $contentWrap = $('.layout__wrap-content');
		var $footer = $('.layout__footer');

		function footerBottom () {
			var winHeight, contentHeight, footerHeight;

			$html.removeClass('is-footer-fixed');
			$contentWrap.css('padding-bottom', '');
			$footer.css('margin-top', '');

			winHeight = $(window).height();
			contentHeight = $contentWrap.outerHeight();
			footerHeight = $footer.outerHeight();


			if((contentHeight + footerHeight) <= winHeight) {
				$html.addClass('is-footer-fixed')
				$contentWrap.css('padding-bottom', footerHeight);
				$footer.css('margin-top', - footerHeight);
			}
		}

		footerBottom();

		$(window).on('resize', footerBottom);
	});
*/

	// Прижимаем футер
	$(function () {
		if($('html').hasClass('is-footer-fixed')) {
			var body = $('body');
			var footer = $('.layout__footer');
			var footerHeight = footer.height() + parseInt(footer.css('margin-top'));
			body.css('margin-bottom', footerHeight);
		}
	});



	$(function () {
		// Модальные окна
		$('body').on('click', 'a.pupop, .js-modal', function (e) {
			var target = $(this).data('href') || $(this).attr('href');
			if(target.substr(0,1) == '#'){
				var $target = $(target);
			} else {
				$.get(target, function(data){
					$target = $(data);
					open();
				});
				return false;
			}

			if($.fancybox.isOpen) {
				$.fancybox.close(true);
				setTimeout(function() {open();}, 250);
			} else {
				open();
			}

			function open() {
				$.fancybox.open(
					$target,
					{
						padding: 0,
						margin: 20,
						closeEffect: 'none',
						wrapCSS: 'is-default',
						closeSpeed: 0,
						openSpeed: 0,
						openEffect: 'none',
						openOpacity: false,
						closeOpacity: false,
						fitToView: true,
						scrolling: 'visible',

						beforeShow: function () {
							$('html').addClass('fancybox-margin fancybox-lock');
							$('.fancybox-wrap').livequery(function(){
								var $context = $(this);
								$('[data-mask="phone"]', $context).each(function(){
									$(this).mask('+7 (999) 999-99-99');
								});
							});
						}
					}
				);
			}

			e.preventDefault();
		});

		// Модальные окна ajax
		$('.js-modal-ajax').on('click',  function() {
			var href =  $(this).attr('href');
			// console.warn('.js-modal-ajax '+href);
			$.fancybox.open(
				{
					href: href
				},
				{
					wrapCSS: 'is-ajax',
					type: 'ajax',
					fitToView: true,
					autoResize: true,
					padding: 20,
					margin: 20,
					maxWidth: 1000,
					afterLoad: function (current, previous) {
						var $content = $(current.content);

						$content.addClass('_ajax-append');
						current.content = $('<div>').append($content.clone()).html();
						$.fancybox.showLoading()
					},
					afterShow: function () {
						$('.fancybox-wrap').livequery();
						setTimeout(function () {
							$('.fancybox-wrap .bem').trigger('resize.block');
							$.fancybox .hideLoading();
							$('.fancybox-wrap').removeClass('is-ajax');
						}, 600);
					}
				}
			);
			return false;
		});

		$('.js-modal-img-ajax').on('click',  function() {
			var href =  $(this).attr('href');
			// console.warn('.js-modal-ajax '+href);
			$.fancybox.open(
				{
					href: href
				},
				{
					wrapCSS: 'is-ajax',
					type: 'image',
					fitToView: true,
					autoResize: true,
					padding: 20,
					margin: 20,
					maxWidth: 1000,
					afterLoad: function (current, previous) {
						var $content = $(current.content);

						$content.addClass('_ajax-append');
						current.content = $('<div>').append($content.clone()).html();
						//$.fancybox.showLoading();
					},
					afterShow: function () {
						$('.fancybox-wrap').livequery();
						setTimeout(function () {
							$('.fancybox-wrap .bem').trigger('resize.block');
							$.fancybox .hideLoading();
							$('.fancybox-wrap').removeClass('is-ajax');
						}, 600);
					}
				}
			);
			return false;
		});

	});

// Плейсхолдеры
	$(function () {
		$('input[placeholder], textarea[placeholder]').placeholder();
	});

// Стилизация селектов
	$(function () {
		$('select.select2').each(function () {
			var placeholder = $(this).attr('placeholder');
			var  templateSR = function (state) {
				var $thstate = $(state.element);

				var $state = $(
					'<span>' + state.text  + '</span>'
				);
				if($thstate.data('img')){
					$('<span class="is-img-point" style="background-image: url('+ $thstate.data('img') +')"></span>').prependTo($state);
				}
				if($thstate.data('format')) $state.addClass('is-format-'+ $(state.element).data('format'));
				return $state;
			}

			$(this).select2({
				minimumResultsForSearch: 8,
				placeholder: placeholder,
				templateResult: templateSR,
				templateSelection: templateSR
			});
		});
	});


// Стилизация радиокнопок и чекбоксов
	$(function () {
		$('input[type="checkbox"], input[type="radio"]').not('.unstyled').each(function(){
			$(this).iCheck();
			$(this).on('ifToggled', function(e){
				$(this).trigger('change').trigger('click'); // Trigger default event
			});
		});
	});

// Скрипт для форм form.result.new
$.fn.initWebForm = function(block, options) {
/*
WP_SUCCESS_MODE

S1 - Редирект на страницу "Спасибо"
S2 - Редирект на текущую страницу (перезагрузка страницы)
S3 - Редирект на текущую страницу с показом информационного попапа, после загрузки страницы
S4 - Замена содержимого формы на сообщение об успешной отправке без изменения размера формы
S5 - Замена содержимого формы на сообщение об успешной отправке с изменением размера формы
S6 - Замена содержимого формы на сообщение об успешной отправке с последующим полным скрытием формы после небольшой паузы (закрыванием попапа).
S7 - Показ сообщения об успешной отправке в попапе и очистка (сброс) формы.
S8 - Показ сообщения об успешной отправке в попапе БЕЗ очистки (сброса) формы.
*/

	var default_options = {
		WP_SUCCESS_MODE: 'S5'
	};
	options = $.extend({}, default_options, options);

	var $form = $(this);
	var blockName = $form.find('input[name=block_name]').val() || block || 'form-standart';
	var $context = $form.closest('.'+blockName);

	var $inputs = $form.find('[data-fieldname]');
	var $fields = $('.' + blockName + '__field', $context);
	var $successMessage = $('.success-message-modal',$context);
	var isAjax = $context.hasClass('js-ajax') || $form.hasClass('js-ajax');

	var MESS = {
		'browser': 'Браузер',
		'windowsize': 'Размер окна',
		'utm_source': 'UTM источник',
		'utm_campaign': 'UTM кампания',
		'utm_medium': 'UTM медиа',
		'utm_keyword': 'UTM ключевые слова',
		'location': 'Текущая страница',
		'message_sent': 'Сообщение отправлено'
	};

	if($form.data('form-initiated') !== undefined) return;
	$form.data('form-initiated', true);

	prepareFieldsValidation();
	initFormSubmit();

	function getFieldsData(){
		var data = {};
		$form.find('[data-fieldname]').each(function(){
			data[$(this).data('fieldname')] = $(this).val();
		});
		return data;
	}

	function getCookie(name) {
	  var matches = document.cookie.match(new RegExp(
	    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	  ));
	  return matches ? decodeURIComponent(matches[1]) : undefined;
	}

	function getUtms() {
		var utm = getCookie('__utmz');
		utms = {};
		if(utm){
			eval(utm.replace(/.*?(utm.*?)=([^|]*)[|]?/g, "utms['$1'] = '$2';\n"));
		}
		return utms;
	}

	function getUserInfo(){
		var userinfo = {};
		userinfo['browser'] = navigator.userAgent;
		userinfo['windowsize'] = $(window).width() + "×" + $(window).height();
		userinfo['location'] = document.location.href;
		utms = getUtms();
		if(utms.utmcsr){
			userinfo['utm_source'] = utms.utmcsr;
		}
		if(utms.utmccn){
			userinfo['utm_campaign'] = utms.utmccn;
		}
		if(utms.utmcmd){
			userinfo['utm_medium'] = utms.utmcmd;
		}
		if(utms.utmcmd){
			userinfo['utm_keyword'] = utms.utmctr;
		}

		var stringresult = "";
		$.each(userinfo, function(key, value){
			stringresult += MESS[key] + ": " + value + "\n";
		});
		return stringresult;
	}

	function prepareFieldsValidation () {
		// Обеспечиваем совместимость с предыдущим решением
		$fields.filter('[data-necessary]').each(function () {
			var $field = $(this);
			var $inputs = $('input, textarea, select', $field);

			$inputs.not('[type="checkbox"]').each(function () {
				var $this = $(this);
				$this.removeAttr('required').data('required', true);

				$this.on('click keydown change', function() {
					$(this).closest('.'+blockName+'__field').removeClass('is-none');
				});
			});
		});

		$('input[data-necessary], textarea[data-necessary], select[data-necessary]', $form).each(function () {
			var $this = $(this);

			$this.data('required', true);

			$this.on('click keydown change', function() {
				$(this).closest('.'+blockName+'__field').removeClass('is-none');
			});
		});

		// Меняем дефолтное поведение обязательных полей
		$('input[required], textarea[required], select[required]', $form).each(function () {
			var $this = $(this);

			$this.removeAttr('required').data('required', true);

			$this.on('click keydown change', function() {
				$(this).closest('.'+blockName+'__field').removeClass('is-none');
			});
		});
	}

	function initFormSubmit () {
		if(isAjax) {
			$form.ajaxForm({
				dataType: (Webprofy.isOldIe() ? 'text' : 'json'),
				beforeSerialize: function(){
					$form.find('input[name="confirm"]').remove();
					$form.find('input[data-fieldname="_utm"]').val(getUserInfo());
				},
				beforeSubmit: function(){
					var preValidation = checkFields();

					/* Очищаем все ошибки формы*/
					var $all_error = $('.'+blockName+'__common-error-placeholder', $context);
					if($all_error.length) $all_error.html('');
					if(preValidation) {
						$form
							.find('input[type="submit"]')
							.prop( "disabled", true)
							.addClass('is-disabled');
					}

					return preValidation;
				},
				success: ajaxSuccessHandler,
				error: ajaxErrorHandler
			});
		} else {
			$form.on('submit', checkFields);
		}
	}

	function ajaxSuccessHandler (data) {
		//console.log(data);

		$form.find('input[type="submit"]').prop( "disabled", false).removeClass('is-disabled');
		$form.find('.error-text').remove();
		$form.find('.is-error').removeClass('is-error');

		if(Webprofy.isOldIe()) {
			eval('var data=' + data);
		}

		// console.log(data);

		if(data.status == 'error'){
			var errorCommonContainer = $('.'+blockName+'__common-error-placeholder', $context);
			$.each(data.errors, function(key, value){
				var $input = $form.find('[data-fieldname="' + key + '"]');
				var $container = $input.closest('.'+blockName+'__field');
				var $error;

				if($container.length > 0){
					$error = $container.find('.'+blockName+'__error');
					if($error.length==0)
						$error = $('<div class="'+blockName+'__error"></div>').appendTo($container);

					$error.text(value);
					$container.addClass('is-error');

					$input.on('change',function(){
						$container.removeClass('is-error');
					});

				} else {
					if(errorCommonContainer.length > 0){
						$container = errorCommonContainer;
					}else{
						$form.append('<div class="'+blockName+'__common-error-placeholder"></div>');
						errorCommonContainer = $('.'+blockName+'__common-error-placeholder', $context);
					}

					$(errorCommonContainer).append(
						'<div class="'+blockName+'__common-error"><div class="'+blockName+'__common-error-text">'  + value + '</div></div>'
					);
				}
			});
			$(document).trigger('webform.error', [$form.attr('name'), data, getFieldsData()]);
		}

		if(data.status == 'success'){
			$(document).trigger('webform.success', [$form.attr('name'), getFieldsData()]);
			//console.log(options.WP_SUCCESS_MODE);
			//console.log(options);

			if (data.message == undefined) {
				data.message = options.WP_DEFAULT_SUCCESS_MESSAGE;
			}
			switch (options.WP_SUCCESS_MODE) {
				case 'S1':
					if(data.redirect) {
						window.location = data.redirect
					} else {
						window.location.reload();
					}
				    break;
				case 'S2':
					window.location.reload();
				    break;
				case 'S3':
					var loc = window.location;
					if(window.location.href.indexOf('?') + 1) {
						var popup_get = '&' + options.WP_FORM_ID + '=Y';
					} else {
						var popup_get = '?' + options.WP_FORM_ID + '=Y';
					}
					window.location = window.location.href + popup_get;
				    break;
				case 'S4':
					// доработать - Замена содержимого формы на сообщение об успешной отправке без изменения размера формы
					$form.parent().html('<div class="success-message">' + data.message + '</div>');
				    break;
				case 'S5':
					$form.parent().html('<div class="success-message">' + data.message + '</div>');
				    break;
				case 'S6':
					// доработать - Замена содержимого формы на сообщение об успешной отправке с последующим полным скрытием формы после небольшой паузы (закрыванием попапа)
					$form.parent().html('<div class="success-message">' + data.message + '</div>');
				    break;
				case 'S8':
					/** Используется в Личном кабинете */
					$.fancybox.close();
					$.fancybox($successMessage , {
						afterClose: function(){
							if($successMessage.hasClass('js-reload-page')){
								window.location = window.location;
							}
						},
						wrapCSS: 'modal-theme',
						autoCenter: false,
						padding: 0,
						fitToView: false
					});
					break;
				case 'S7':
				default:
					$.fancybox.close();
					$.fancybox($successMessage , {
						afterClose: function(){
							if($successMessage.hasClass('js-reload-page')){
								window.location = window.location;
							}
							$context.find('form').trigger('reset'); // todo: доработать - сброс формы на дефолтное состояние
						},
						wrapCSS: 'modal-theme',
						autoCenter: false,
						padding: 0,
						fitToView: false
					});
				    break;

			}

			/*
				if(data.redirect){
					document.location = data.redirect;
				} else if(data.message) {
					$form.parent().html('<div class="success-message">' + data.message + '</div>');
				} else {
					$.fancybox($successMessage , {
						afterClose: function(){
							if($successMessage.hasClass('js-reload-page')){
								window.location = window.location;
							}
						},
						wrapCSS: 'modal-theme',
						autoCenter: false,
						padding: 0,
						fitToView: false
					});
				}
			*/
		}
	}

	function ajaxErrorHandler (data) {
	//	console.log(data);

		if(typeof window.isLocalBuild !== 'undefined') {
			data.status = 'success';
			ajaxSuccessHandler(data);
		} else {
			$form.find('input[type="submit"]').prop( "disabled", false).removeClass('disabled');
			$form.find('.is-captcha-field').addClass('is-error');
		}
	}

	function checkFields () {
		var result = true;

		$fields.each(function () {
			var $field = $(this);
			var $inputs = $('input, textarea, select', $field);
			if($field.hasClass('is-error') || $field.hasClass('is-none')) {
				result = false;
				return;
			}

			$inputs.each(function () {
				var $input = $(this);
				var checkResult = true;
				var isContinue = true;

				if($input.data('required') == undefined) return;

				switch($input.attr('type')) {
					case 'checkbox':
						if(!$input.data('required')) {
							checkResult = true && checkResult;
						} else if($input.val() == '') {
							checkResult = false;
							$field.addClass('is-none');
						}
						result = checkResult && result;
						break;

					case 'radio':
						if(!$input.data('required')) {
							checkResult = true && checkResult;
						} else if($inputs.filter(':checked').length == 0) {
							checkResult = false;
							$field.addClass('is-none');
						}
						result = checkResult && result;
						break;

					default:
						if(!$input.data('required')) {
							checkResult = true && checkResult;
						} else if($input.val().trim() == '') {
							checkResult = false;
							$field.addClass('is-none');
						}
						result = checkResult && result;
						break;
				}
			});
		});

		return result;
	}
};

//Обновляем последние просмотренные товары
function updateViewList(site_id, product_id, parent_id) {
	var href="/bitrix/components/bitrix/catalog.element/ajax.php";
	$.ajax({
		type: "POST",
		url: href,
		dataType: "text",
		data: {
			AJAX: "Y",
			SITE_ID: site_id,
			PRODUCT_ID: product_id,
			PARENT_ID: parent_id
		}
	}).done(function(resp) {
		//Thanx, bitrix
		eval('var response='+resp);
		if(response.STATUS == 'SUCCESS') {
			// console.log('updateViewList.ok');
		}else{
			console.warn('updateViewList.error');
		}
	});
}

// контент зависимый от чекбокса .show-on-condition
function initShowOnCondition(){
	$('[data-watch]').each(function(){
		var $this = $(this),
			watch = $this.attr('data-watch'),
			$that = $(watch);

		var change = $this.attr('data-change');
		if(change){
			$that.change(function(){
				try{
					eval(change);
				}
				catch(e){
					console.error('bad data-change');
				}
			}).eq(0).trigger('change');
			return;
		}

		var showSelectEquals = $this.attr('data-show-select-equals');
		if(showSelectEquals){
			$this.change(function(){
				$that.filter(':checked').val('')
			})
		}
	});

	var listeners = [];

	$('.show-on-condition').each(function(){
		var self = $(this);
		var condition = self.data('condition');

		// Complex parser :)
		var conditions = condition.split(" and ");

		$.each(conditions, function(key, value){
			var operands = value.split(" ");
			if(operands.length == 3){
				listeners.push({
					input: operands[0],
					conditions: conditions,
					object: self
				});
			}
		});
	});

	$(document).on('change', 'input, textarea, select', function(){
		var name = $(this).attr('name');
		updateConditions(name);
	});

	function updateConditions(name){
		$.each(listeners, function(key, value){

			if(typeof(name) === 'string' && value.input != name)
				return; // skip this one;

			var result = true;
			$.each(value.conditions, function(k, v){
				var operands = v.split(" ");
				if(operands.length == 3){
					var input = $('[name="' + operands[0] + '"]');
					if(input.length >= 1){
						var input_value;
						if(input.length == 1){
							input_value = input.val();
						} else {
							input_value = input.filter(':checked').val();
						}

						if(input_value == operands[2]){
							result = result & (operands[1] == "is");
						} else {
							result = result & (operands[1] == "not");
						}
					} else {
						result = false;
					}
				} else {
					result = false;
				}
			});
			if(result){
				value.object.show();
			} else {
				value.object.hide();
			}
		});
	}
	updateConditions();
}
$(function(){
	initShowOnCondition();
});

// $(function(){
// 	// Content table wrapper
// 	$('.content-area table').each(function(){
// 		$(this).wrap('<div class="layout__content-table"></div>');
// 	});
// });

function plural(n,f){n%=100;if(n>10&&n<20)return f[2];n%=10;return f[n>1&&n<5?1:n==1?0:2]}



$(document).ready(function(){
	$('.main-slider').slick({
		prevArrow: '<button type="button" class="slick-prev icon-arrow"></button>',
		nextArrow: '<button type="button" class="slick-next icon-arrow"></button>',
		dots: true,
		dotsClass: 'main-dots',
		appendArrows: $(".arrow"),
		appendDots: $('.dots'),
		adaptiveHeight: false
});

});

$(document).ready(function(){
	$('.project-slider').slick({
		prevArrow: '<button type="button" class="slick-prev icon-arrow"></button>',
		nextArrow: '<button type="button" class="slick-next icon-arrow"></button>',
		dots: true,
		dotsClass: 'project-dots',
		appendArrows: $(".arrow-project"),
		appendDots: $('.dots-project'),
		responsive: [{

		}

		]
	});

});

$(document).ready(function(){
	$('.reason-slider').slick({
		prevArrow: '<button type="button" class="slick-prev icon-arrow"></button>',
		nextArrow: '<button type="button" class="slick-next icon-arrow"></button>',
		dots: true,
		dotsClass: 'reason-dots',
		appendArrows: $(".arrow-reason"),
		appendDots: $('.dots-reason')
	});

});

$(document).ready(function(){
	$('.client-slider__container').slick({
		slidesToShow: 4,
		prevArrow: '<button type="button" class="slick-prev icon-arrow slick-arrow"></button>',
		nextArrow: '<button type="button" class="slick-next icon-arrow slick-arrow"></button>',
		responsive: [
			{
				breakpoint: 1248,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1
				}
			}

		]
	});

});

$(document).ready(function(){
	$('.content-slider__container').slick({
		slidesToShow: 4,
		prevArrow: '<button type="button" class="slick-prev icon-arrow slick-arrow"></button>',
		nextArrow: '<button type="button" class="slick-next icon-arrow slick-arrow"></button>',
		responsive: [
			{
				breakpoint: 1248,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1
				}
			}

		]
	});

});

$(document).ready(function () {
		$(".main-slider__text").css({"opasity":"0"});
});

$(document).ready(function() {
	$('.open-modal').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});
});

$(document).ready(function() {
	$(".open-m").fancybox({
		width: false,
		autoScale: false
	});
});


$(document).ready(function(){


		$('.footer__arrow-up').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 700);
			return false;
		});

	});

$(document).ready(function(){


	$("#modal-consultation .button-primary").on("click", function(){
		$("html").addClass("lock");
	});

	$(".main").append('<div class="fancybox-overlay fancybox-overlay-fixed" style="width: auto; height: auto; display: block;"></div>')





	$(".fancybox-close").on("click", function(){
		$("html").removeClass("lock");
	})

	/*
	$(".header-mobile__close").on("click", function(){
		$("#mobile-menu").removeClass("is-active");
	});
	$(".header-mobile__menu-button").on("click", function(){
		$("#mobile-menu").addClass("is-active");
	});

	$(".menu-tree__link").on("click", function(){
		$(".header-mobile__menu-left").addClass("is-active");
	});
	$(".header-mobile__menu-left-close").on("click", function(){
		$(".header-mobile__menu-left").removeClass("is-active");
	});

	$(".header-mobile__menu-button").on("click", function () {
		$("html").addClass("header-mobile-lock")
	})
	$(".header-mobile__close").on("click", function () {
		$("html").removeClass("header-mobile-lock")
	})
	*/

	$('.consultation-btn').click(function () {
		hasBeenClicked = true;
	});
		if ($("body").find(".success-message-modal") || hasBeenClicked) {
			$(".fancybox-overlay").addClass("thank3")
		}







	$('.open-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return item.el.attr('title') + '  <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank"></a>';
			}
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300,
			opener: function(element) {
				return element.find('img');
			}
		}

	});






	$(document).ready(
		function() {
			$(".rewards__block").niceScroll({cursorcolor:"transparent", cursorborder: "none"});
		}
	);


	/*
	var dilersMap;
	ymaps.ready(function () {
		dilersMap = new ymaps.Map("YMapsID", {
			center: [55.76, 37.64],
			zoom: 10
		});

	});
	*/










	$(document).mouseup(function (q) {
		var container = $(".fancybox-outer");
		if (container.has(q.target).length === 0){
			$("html").removeClass("lock");
		}
	});





	$(".open-modal").on("click", function(){
		$(".mfp-bg").addClass("login-back");
	});
	$(".consultation__btn").on("click", function(){
		$(".mfp-bg").addClass("consultation-back");
	});




});

$(document).ready(function(){
	$('.accordeon-btn').click(function(){
		$(this).toggleClass('is-active');
		$(this).next('.accordeon-list').slideToggle(400);
	});

	// Перенесены в функции для обработки элементов подгруженных через ajax
	//var newsTile = $(".news-tiles__item");
	//var newsList = $(".news-list__item");
	//var tileItem = $(".tile__item");
	//var projectTile= $(".project-tile__inner");

	var winHeight = $(window).height() / 2;

	function getPositionNewsTile(){
		var newsTile = $(".news-tiles__item");
		newsTile.each(function(){
			var _x = this.getBoundingClientRect().top,
				_h = $(this).height();
			if (winHeight > _x ){
				$(this).addClass('is-active');

				if (_x + _h < 100){
					$(this).removeClass('is-active')
				}
			} else if (winHeight < _x){
				$(this).removeClass('is-active')
			}
		});
	}

	function getPositionNewsList(){
		var newsList = $(".news-list__item");
		newsList.each(function(){
			var _x = this.getBoundingClientRect().top,
				_h = $(this).height();
			if (winHeight > _x ){
				$(this).addClass('is-active');

				if (_x + _h < 100){
					$(this).removeClass('is-active')
				}
			} else if (winHeight < _x){
				$(this).removeClass('is-active')
			}
		});
	}

	function getPositionTile(){
		var tileItem = $(".tile__item");
		tileItem.each(function(){
			var _x = this.getBoundingClientRect().top,
				_h = $(this).height();
			if (winHeight > _x ){
				$(this).addClass('is-active');

				if (_x + _h < 100){
					$(this).removeClass('is-active')
				}
			} else if (winHeight < _x){
				$(this).removeClass('is-active')
			}
		});
	}

	function getPositionProject(){
		var projectTile= $(".project-tile__inner");
		projectTile.each(function(){
			var _x = this.getBoundingClientRect().top,
				_h = $(this).height();
			if (winHeight > _x ){
				$(this).addClass('is-active');

				if (_x + _h < 100){
					$(this).removeClass('is-active')
				}
			} else if (winHeight < _x){
				$(this).removeClass('is-active')
			}
		});
	}



	$(window).on('scroll', getPositionNewsTile);
	$(window).on('scroll', getPositionNewsList);
	$(window).on('scroll', getPositionTile);
	$(window).on('scroll', getPositionProject);


});











