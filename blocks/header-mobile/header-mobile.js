$(document).ready(function(){
	var $body = $('body'),
		$navBurger = $('.header-mobile__burger'),
		$headerMenu = $('.header-menu'),
		$headerMenuCompany = $('.header-menu-company'),
		$headerMenuCompanyAbout = $('.header-menu-company-about'),
		$headerMenuCompanyBranches = $('.header-menu-company-branches'),
		$headerMenuCompanyGalery = $('.header-menu-company-galery'),
		$headerMenuCommand = $('.header-menu-command'),
		$headerMenuInfocenter = $('.header-menu-infocenter'),
		$headerMenuContact = $('.header-menu-contact');


	// Открытие меню, добавление в body overflow: hidden
	$body.on('click', '.header-mobile__burger', function (e) {
		$navBurger.toggleClass('open');
		$body.toggleClass('mobile-menu');
		$headerMenu.toggleClass('open');

		if ($headerMenuCompany.hasClass('open')) {
			$headerMenuCompany.removeClass('open');
		}

		if ($headerMenuCompanyAbout.hasClass('open')) {
			$headerMenuCompanyAbout.removeClass('open');
		}

		if ($headerMenuCompanyBranches.hasClass('open')) {
			$headerMenuCompanyBranches.removeClass('open');
		}

		if ($headerMenuCompanyGalery.hasClass('open')) {
			$headerMenuCompanyGalery.removeClass('open');
		}

		if ($headerMenuCommand.hasClass('open')) {
			$headerMenuCommand.removeClass('open');
		}

		if ($headerMenuInfocenter.hasClass('open')) {
			$headerMenuInfocenter.removeClass('open');
		}

		if ($headerMenuContact.hasClass('open')) {
			$headerMenuContact.removeClass('open');
		}
	});

	function toggleMenu(openMenu, setComponent, closeMenu) {
		$body.on('click', openMenu, function (e) {

			if(!setComponent.hasClass('open')) {
				setComponent.addClass('open');
			}

			$body.on('click', closeMenu, function (e) {
				if (setComponent.hasClass('open')) {
					setComponent.removeClass('open');
				}
			});
		});
	}

	// Меню О компании
	toggleMenu('.header-menu__first-next-level.company', $headerMenuCompany, '.header-menu-company > .header-menu__first-level > .header-menu__first-item.back');
	// О нас
	toggleMenu('.header-menu-company .header-menu__first-next-level.about', $headerMenuCompanyAbout, '.header-menu-company-about > .header-menu__first-level > .header-menu__first-item.back');
	// Филиалы
	toggleMenu('.header-menu-company .header-menu__first-next-level.branches', $headerMenuCompanyBranches, '.header-menu-company-branches > .header-menu__first-level > .header-menu__first-item.back');
	// Галерея
	toggleMenu('.header-menu-company .header-menu__first-next-level.galery', $headerMenuCompanyGalery, '.header-menu-company-galery > .header-menu__first-level > .header-menu__first-item.back');
	// Команда
	toggleMenu('.header-menu__first-next-level.command', $headerMenuCommand, '.header-menu-command > .header-menu__first-level > .header-menu__first-item.back');
	// Инфоцентр
	toggleMenu('.header-menu__first-next-level.infocenter', $headerMenuInfocenter, '.header-menu-infocenter > .header-menu__first-level > .header-menu__first-item.back');
	// Контакты
	toggleMenu('.header-menu__first-next-level.contact', $headerMenuContact, '.header-menu-contact > .header-menu__first-level > .header-menu__first-item.back');
});